<?php

defined('ABSPATH') || die('Not allowed');

add_action('admin_init', function() {
	add_settings_section( 'block-comments-by-characterset', 'Block Comments by Character Set', 'jp_bcbl_settingsSection', 'discussion' );

	add_settings_field( 'bcbl_actionOnMatch', 'Action on match', 'jp_bcbl_settingsField_action', 'discussion', 'block-comments-by-characterset' );
	add_settings_field( 'bcbl_characterSetsToMatch', 'Which character sets should be blocked', 'jp_bcbl_settingsField_characterSets', 'discussion', 'block-comments-by-characterset' );

	register_setting('discussion', 'bcbl_characterSetsToMatch', array(
		'default' => array(),
	));
	register_setting('discussion', 'bcbl_actionOnMatch', array(
		'default' => 'spam',
	));
});


function jp_bcbl_settingsSection () {

}

function jp_bcbl_settingsField_action () {
	$action = JakeParis\BlockCommentsByCharacterSet\markStatusAs();
	echo '<p>What should be done with comments that match one of the character set filters? Note that if you choose <em>block completely</em>, the user will receive a totally blank screen upon submission instead of reloading the same page.</p>

		<input type="radio" name="bcbl_actionOnMatch" value="spam" id="bcbl_actionOnMatch-spam" '.checked( 'spam', $action,0 ).'>
		<label for="bcbl_actionOnMatch-spam">Mark as spam</label>
		<br>
		<input type="radio" name="bcbl_actionOnMatch" value="unapproved" id="bcbl_actionOnMatch-unapproved" '.checked( 'unapproved', $action,0 ).'>
		<label for="bcbl_actionOnMatch-unapproved">Mark as pending</label>
		<br>
		<input type="radio" name="bcbl_actionOnMatch" value="kill" id="bcbl_actionOnMatch-kill" '.checked( 'kill', $action,0 ).'>
		<label for="bcbl_actionOnMatch-kill">Block completely</label>
		<br>
	';
}

function jp_bcbl_settingsField_characterSets () {
	$languages = JakeParis\BlockCommentsByCharacterSet\allCharacterSets();
	$blocked = JakeParis\BlockCommentsByCharacterSet\characterSetsToBlock();

	echo '<p>If any of these character sets is detected anywhere in the comment, the whole comment will be flagged as noted above.<ul style="columns: 15em;">';
		foreach($languages as $lan){
			$label = str_replace('_', ' ', $lan);
			$id = "bcbl_characterSetsToMatch-{$lan}";
			$checked = in_array($lan, $blocked) ? ' checked ' : '';
			echo '<li><input type="checkbox" name="bcbl_characterSetsToMatch[]" 
				'.$checked.' id="'.$id.'" value="'.$lan.'">
				<label for="'.$id.'">' . $label .'</label>'; 
		}
	echo '</ul>';
}
