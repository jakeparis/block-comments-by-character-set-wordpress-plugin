<?php
namespace JakeParis\BlockCommentsByCharacterSet;


/**
 * Get an array of languages to block.
 * 
 * @return array list of languages indentifiers
 */
function characterSetsToBlock () {
	$default = array('Cyrillic');
	return get_option('bcbl_characterSetsToMatch', $default );
}

/**
 * Get an array of all langugages available to look for.
 *
 * This list is based on https://www.php.net/manual/en/regexp.reference.unicode.php
 * under the Supported Scripts section.
 * 
 * @return array list of languages identifiers
 */
function allCharacterSets () {
	return array(
		'Arabic',
		'Armenian',
		'Avestan',
		'Balinese',
		'Bamum',
		'Batak',
		'Bengali',
		'Bopomofo',
		'Brahmi',
		'Buginese',
		'Buhid',
		'Chakma',
		'Cham',
		'Cherokee',
		'Coptic',
		'Cypriot',
		'Cyrillic',
		'Deseret',
		'Devanagari',
		'Ethiopic',
		'Georgian',
		'Glagolitic',
		'Gothic',
		'Greek',
		'Gujarati',
		'Gurmukhi',
		'Han',
		'Hangul',
		'Hanunoo',
		'Hebrew',
		'Hiragana',
		'Imperial_Aramaic',
		'Javanese',
		'Kaithi',
		'Kannada',
		'Katakana',
		'Kayah_Li',
		'Kharoshthi',
		'Khmer',
		'Lao',
		'Latin',
		'Lepcha',
		'Limbu',
		'Lisu',
		'Lycian',
		'Lydian',
		'Malayalam',
		'Mandaic',
		'Meetei_Mayek',
		'Miao',
		'Mongolian',
		'Myanmar',
		'New_Tai_Lue',
		'Nko',
		'Ogham',
		'Ol_Chiki',
		'Oriya',
		'Osmanya',
		'Phags_Pa',
		'Phoenician',
		'Rejang',
		'Runic',
		'Samaritan',
		'Saurashtra',
		'Sharada',
		'Sinhala',
		'Sora_Sompeng',
		'Sundanese',
		'Syloti_Nagri',
		'Syriac',
		'Tagalog',
		'Tagbanwa',
		'Tai_Le',
		'Tai_Tham',
		'Tai_Viet',
		'Takri',
		'Tamil',
		'Telugu',
		'Thaana',
		'Thai',
		'Tibetan',
		'Tifinagh',
		'Ugaritic',
		'Vai',
		'Yi',
	);
}

function markStatusAs () {
	// return 'unapproved' to put in pending queue
	//        'spam' to put in spam folder
	//        anything else to completely block comment
	// return 'kill';
	return get_option( 'bcbl_actionOnMatch', 'spam' );
}

function getCharacterSetTestRegex () {
	$lans = characterSetsToBlock();
	$lans = array_map(function($v){
		return '\p{'.$v.'}';
	}, $lans);
	$lans = join('|',$lans);
	// u is for unicode mode, a necessity here
	$regex = '#(?:' . $lans . ')#u';

	return $regex;
}