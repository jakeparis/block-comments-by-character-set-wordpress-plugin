
# Block Comments by Character Set

Contributors: jakeparis
Donate link: https://jake.paris
Tags: spam, comments, cyrillic, block
Requires at least: 5.3
Tested up to: 5.3
Stable tag: 1.0.0

This plugin allows blocking comments based on the character set they're written in.

## Description

This plugin allows blocking comments based on the character set they're written in.


## Changelog

### 1.0.0
Initial development version.
