<?php
namespace JakeParis\BlockCommentsByCharacterSet;
/*
Plugin Name: Block Comments by Character Set
Plugin URI:  https://jake.paris/wordpress-plugins/
Description: Block comments by the character set they're written in. For example,
             block any comments which are written in Cyrillic.  
Version:     1.0.0
Author:      Jake Paris
Author URI:  https://jake.paris/
License:     MIT
License URI: https://opensource.org/licenses/MIT
*/

require_once plugin_dir_path(__FILE__) . 'functions.php';
require_once plugin_dir_path(__FILE__) . 'settings.php';


function filterComment ($approvedStatus, $commentdata) {

	// return 'spam' (mark as spam)
	// return 1 (mark as approved)
	// return 0 (mark pending)
	// return WP_Error to shortcircuit rest of comment checks and block insertion. Will return whitescreen to user.

	$languageIdentifiers = characterSetsToBlock();
	if( empty($languageIdentifiers) )
		return $approvedStatus;

	foreach($languageIdentifiers as $lan) {
		$regex = getCharacterSetTestRegex();
		$match = preg_match($regex, $commentdata['comment_content']);
		if( $match === 1) {
			$markStatusAs = markStatusAs();
			switch( $markStatusAs ) {
				case 'spam' :
					return 'spam';
				case 'unapproved':
					return 0;
				default :
					return new \WP_Error('Disallowed comment');
			}
		}
	}
	return $approvedStatus;

}
add_filter('pre_comment_approved', __NAMESPACE__.'\filterComment', 99, 2);


register_deactivation_hook(__FILE__, function(){
	delete_option( 'bcbl_characterSetsToMatch');
	delete_option( 'bcbl_actionOnMatch');
});